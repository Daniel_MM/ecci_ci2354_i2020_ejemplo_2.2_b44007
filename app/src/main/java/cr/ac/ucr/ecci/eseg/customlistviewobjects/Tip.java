package cr.ac.ucr.ecci.eseg.customlistviewobjects;

import androidx.annotation.NonNull;

public class Tip {
    private final String name;
    private final String img;
    private final String description;
    Tip(String name, String img, String description) {
        this.name = name;
        this.img = img;
        this.description = description;
    }
    public String getName() {
        return name;
    }
    public String getImg() {
        return img;
    }
    public String getDescription() {
        return description;
    }

    // Para el ListView
    @NonNull
    @Override
    public String toString() {
        return this.name + ": " +this.description;
    }
}
