package cr.ac.ucr.ecci.eseg.customlistviewobjects;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.list);
        List<Tip> mTips = new ArrayList<>();
        mTips.add(new Tip("Agua", "01", "Al menos 8 vasos al día"));
        mTips.add(new Tip("Vino", "02", "No exceda una copa al día"));
        mTips.add(new Tip("Café", "03", "Evite tomarlo"));
        mTips.add(new Tip("Carnes", "04", "Al menos tres veces a la semana"));
        mTips.add(new Tip("Hamburguesa", "05", "Solo caseras y bajas en grasa"));

        ArrayAdapter<Tip> adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, android.R.id.text1, mTips);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Tip item = (Tip)listView.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(), "Position: " + position +
                        " ListItem: " + item.getName(), Toast.LENGTH_LONG).show();
            }
        });
    }
}